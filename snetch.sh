#!/bin/bash
#
# snetch.sh - SSL Ncat Chat
# 	by: Chris Blankenship <cblankenship@pm.me>
# 	-
# 	Disposable, SSL protected, ncat-based chat server
#
# # # #
# Style Variables
# # # #
BOLD=$(tput bold)
NORMAL=$(tput sgr0)
# # # #
# Generate SSL Cert
# # # #
CUR_DIR=$(pwd)
TIMESTAMP=$(date +"%Y%m%d%H%M%S")
yes "  " | openssl req -newkey rsa:4096 -x509 -sha256 -days 7300 -nodes -out $CUR_DIR/$TIMESTAMP.crt -keyout $CUR_DIR/$TIMESTAMP.key 2>/dev/null 1>&2
trap "rm -f $CUR_DIR/$TIMESTAMP.crt $CUR_DIR/$TIMESTAMP.key" EXIT
# # # #
# Print Server Info
# # # #
IP4_ADDRESS=$(curl -m 5 ipv4.icanhazip.com 2>/dev/null || echo "False")
IP6_ADDRESS=$(curl -m 5 ipv6.icanhazip.com 2>/dev/null || echo "False")
RANDOM_PORT=$(comm -23 <(seq 5001 49151 | sort) <(ss -Htan | awk '{print $4}' | cut -d':' -f2 | sort -u) | shuf | head -n 1)
FINGERPRINT=$(openssl x509 -noout -fingerprint -in $TIMESTAMP.crt | awk -F= '{ print $2 }')
[[ $IP4_ADDRESS != *"False"* ]] && echo -e "${BOLD}IPv4 Address: ${NORMAL}$IP4_ADDRESS"
[[ $IP6_ADDRESS != *"False"* ]] && echo -e "${BOLD}IPv4 Address: ${NORMAL}$IP6_ADDRESS"
echo "Port: $RANDOM_PORT"
echo "SSL Fingerprint: $FINGERPRINT"
echo ""
[[ $IP4_ADDRESS != *"False"* ]] && echo "${BOLD}Conncect via IPv4: ${NORMAL}ncat -4 -v --ssl $IP4_ADDRESS $RANDOM_PORT"
[[ $IP6_ADDRESS != *"False"* ]] && echo "${BOLD}Conncect via IPv6: ${NORMAL}ncat -6 -v --ssl $IP6_ADDRESS $RANDOM_PORT"
# # # #
# Start Ncat Chat Server via SSL
# # # #
ncat -l -v --chat --ssl --ssl-key $TIMESTAMP.key --ssl-cert $TIMESTAMP.crt -p $RANDOM_PORT &> $CUR_DIR/$TIMESTAMP.log
