# Misc Perl One-liners

# Replace all Ss at the end of words with Zs
perl -p -e "s/(?<=[\w\'])s(?=[\p{P}\s]*)(?\!\w+)/z/g"
# Example: `echo "What's up, Broseph Tito?" | perl -p -e "s/(?<=[\w\'])s(?=[\p{P}\s]*)(?\!\w+)/z/g"`
# Output: What'z up, Broseph Tito?

# Is a number Prime?
perl -e 'if ( ( "1"x@ARGV[0] ) =~ /^1?$|^(11+?)\1+$/ ) { print "False\n" } else { print "True\n" };'
# Example: perl -e 'if ( ( "1"x@ARGV[0] ) =~ /^1?$|^(11+?)\1+$/ ) { print "False\n" } else { print "True\n" };' 19
# Output: True
