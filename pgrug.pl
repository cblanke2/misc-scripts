#!/usr/bin/env perl

#  pgrug.pl - Poorly written Perl client for 'GrugNet'
#
#  Copyright (c) 2024, Chris Blankenship <cblankenship@pm.me>
#  All rights reserved.
# 
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are
#  met:
#  
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following disclaimer
#    in the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of the  nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#  
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Fail on error
use strict;
use warnings;

# Dependencies
use LWP::UserAgent; # For making/recieving HTTP/REST requests

# Global vars
our $game_server; # URL of GrugNet game server
our $auth_token; # Token used to log in to above server

# # # # 
# name:		game_help
# purpose:	Print help text for game client
# # # # 
sub game_help{
	 print << 'END'
NAME:

	pgrug.pl		Poorly written Perl client for 'GrugNet'; a REST API-based 
				multiplayer game.

AUTHORS:

	Written by		Chris Blankenship <cblankenship\@pm.me>
				www.chrisblankenship.lol

NOTES:

	I wrote this only to prove a point. I'm not sure what that point is though.
	Perhaps that I am unhinged? Probably. Anyways, you can do the same thing that
	this program does in any terminal with a few 'curl' commands. It would likely
	be more user friendly that way too.

	All of the following options are required for this client to, well, be a client,
	but they are considered as optional because you don't need to set them via the.
	CLI. You'll get interactive text to set any that are left unset. Gross, right?

OPTIONS:

	--server, -s		'https://grugnet.tld'

		Optional. Only one instance allowed. The server hosting the instance of
		'GrugNet' you want to connect to. If this is not passed via the CLI, you'll
		get some interactive text to specify the server.

	--token, -t		'LeeroyJenkinsCorrectHorseBatteryStaple'

		Optional. Only one instance allowed. Your auth token on the server hosting 
		the instance of 'GrugNet' you want to connect to. If this is not passed via
		the CLI, you'll get some interactive text to specify the auth token.
	
	--new, -n
	
		Optional, but Required if no account on the specified server exists.
		Starts the process to create a new user. Will ignore any value passed for
		'--token', but will use value passed for '--server' if one exists;
		otherwise you'll get some interactive text to specify the server.

	--help, -h

		Prints this help then exits. Will ignore any other arguments.

EXAMPLES:

	perl pgrug.pl
	perl pgrug.pl --server 'https://grugnet.tld'
	perl pgrug.pl --server 'https://grugnet.tld' --token 'LeeroyJenkinsCorrectHorseBatteryStaple'
	perl pgrug.pl --server 'https://grugnet.tld' --new

COPYRIGHT:

	Copyright (c) 2024, Chris Blankenship <cblankenship@pm.me>. All rights reserved.
	Licensed under the BSD License 2.0 (3-clause BSD License). This is free software;
	you are free to change and redistribute it. THERE IS NO WARRANTY.
END
}

# # # # 
# name:		call_api
# purpose:	Make the actual API requests
# params:	$formatted_command	-->	Reference to a hash containing
#						authentication information (required)
# # # # 
sub call_api {
	# Access global vars
	our $game_server; # URL of GrugNet game server
	our $auth_token; # Token used to log in to above server
	# Get formatted command hash
	my $formatted_command = shift;
	# Construct Header
	my @api_header = [
		'Authorization' => "Bearer $auth_token",
		'Content-Type' => 'application/json; charset=UTF-8'
	];
	# Declare other local vars
	my @api_params;
	my $post_response;
	# Create object for API calls
	my $api_request_object = LWP::UserAgent->new();
	# Type of API Call
	# GET
	if ( %formatted_command{'get_or_post'} eq "GET" ) {
		# Format URL Params for GET Request
		# // TODO
		# Make GET Request
		my $post_response = $api_request_object->get(
			$game_server, 
			@api_header,
			@api_params
		);
	}
	# POST
	elsif ( %formatted_command{'get_or_post'} eq "POST" ) {
		# Format URL Params for POST Request
		# // TODO
		# Make POST Request
		my $post_response = $api_request_object->post(
			$game_server, 
			@api_header,
			@api_params
		);
	} # END "Type of API Call"
	# If response was successful
	if ($post_response->is_success) {
		# Print decoded content of body
		print $post_response->decoded_content;
	}
	# Otherwise...
	else {
		print "NON-FATAL ERROR: server responding with '" . $post_response->status_line . "' error \n";
	} # End "If response was successful"
}

# # # # 
# name:		command_parse
# purpose:	Parse commands sent by 'command_shell'  and format
#			them into proper API calls
# params:	$this_command		-->	String containing command to be
#							parsed (required)
# # # #
sub command_parse {
	# Get passed user input
	my $this_command = shift;
	# Declare hash for storing formatted command
	my %formatted_command;
	# // TODO
	# Format command for API call
	# Pass formatted command to API call function
	call_api \%formatted_command;
}

# # # # 
# name:		command_shell
# purpose:	Take interactive user input, sanitize it, and pass it to
#			the 'command_parse' function for parsing/formatting
# # # # 
sub command_shell {
	# Maintain infinite loop until broken
	while ( 1 == 1 ) {
		# Get user input
		print "\n> ";
		my $this_command = <STDIN>;
		# Sanitize user input
		chomp $this_command;
		# Check for built-in commands
		# Exit
		if ( ( $this_command eq "exit" ) or ( $this_command eq "quit" ) ) {
			# Break loop
			last;
		}
		# Help
		elsif ( ( $this_command eq "help" ) or ( $this_command eq "info" ) ) {
		}
		# If not...
		else {
			# Pass user input to command parser
			command_parse $this_command;
		} # End "# Check for built-in commands"
	} # End "Maintain infinite loop until broken"
	# Close after exiting loop
	exit;
}

# # # # 
# name:		auth_user
# purpose:	Authenticate user against the specified server with
#			the provided token, or create a new user
# params:	$auth_params		-->	Reference to a hash containing
#						authentication information (required)
# # # # 
sub auth_user {
	# Declare local vars for server auth
	my $this_game_server;
	my $this_auth_token;
	# Get hash of CLI Args/Options
	my $auth_params = shift;
	# If game server was not defined by CLI Args/Options
	if ( ! %$auth_params{'server'} ) {
		# Get game server from user input
		print "Server: ";
		$this_game_server = <STDIN>;
		# Sanitize user input
		chomp $this_game_server;
		# Is user input empty?
		if ( $this_game_server =~ /^\s*\n*$/ ) {
			# Throw fatal error and exit
			die "FATAL ERROR: server cannot be empty!\n";
		} # End "Is user input empty?"
	}
	# Otherwise...
	else {
		$this_game_server = %$auth_params{'server'};
	} # End "If game server was not defined by CLI Args/Options"
	# If a new user needs to be created
	if ( %$auth_params{'new'} ) {
	}
	# Otherwise...
	else {
		# If auth token was not defined by CLI Args/Options
		if ( ! %$auth_params{'token'}  ) {
			# Get auth token from user input
			print "Auth Token: ";
			$this_auth_token = <STDIN>;
			# Sanitize user input
			chomp $this_auth_token;
			# Is user input empty?
			if ( $this_auth_token =~ /^\s*\n*$/ ) {
				# Throw fatal error and exit
				die "FATAL ERROR: auth token cannot be empty!\n";
			} # End "Is user input empty?"
		}
		# Otherwise...
		else {
			$this_game_server = %$auth_params{'token'};
		} # End "If auth token was not defined by CLI Args/Options"
	} # End "If a new user needs to be created"
	# Attempt auth with those credentials
	# ////////
	# // TODO AUTH
	# ////////
	# Attempt to auth
	if ( 1 == 1 ) {
		# If Auth worked, save those vars for future use
		our $game_server = $this_game_server;
		our $auth_token = $this_auth_token;
		# Launch command shell
		command_shell;
	}
	# But if auth failed
	else {
		# Throw fatal error and exit
		die "FATAL ERROR: authentication failed!\n";
	} # End "Attempt to auth"
	# ////////
}

# # # # 
# name:		arg_parse
# purpose:	Parse CLI arguments/options
# # # # 
sub arg_parse {
	# CLI Args/Options
	my @arguments = @_;
	# Position in list of CLI Args/Options
	my $arg_position = 0;
	# Dictionary/Hash of auth params
	my %auth_params;
	# Loop through all arguments
	while ( @arguments > $arg_position ) {
		# Help Info
		if ( ( $arguments[$arg_position] eq "--help" ) or ( $arguments[$arg_position] eq "-h" ) ) {
			game_help;
			exit;
		} # End "Help Info"
		# Game Server
		elsif ( ( $arguments[$arg_position] eq "--server" ) or ( $arguments[$arg_position] eq "-s" ) ) {
			# Increment '$arg_position' by one
			$arg_position++;
			# Make sure '--server' value is not empty and matches RegEx for host
			if ( ( $arguments[$arg_position] !~ /^\s*\n*$/ ) and ( ( $arguments[$arg_position] =~ /\w+/ ) or ( $arguments[$arg_position] =~ /\d+/ ) ) ){
				# If '--server' has already been set
				if ( $auth_params{'server'} ) {
					# Throw fatal error and exit
					die "FATAL ERROR: value for '--server' already set";
				}
				# Otherwise...
				else {
					# Add value for 'host' to 'auth_params' dict/hash
					$auth_params{'server'} = $arguments[$arg_position];
				} # End "If '--server' has already been set"
			} # End "Make sure '--server' value is not empty and matches RegEx for host"
		} # End "Game Server"
		# Game Auth Token
		elsif ( ( $arguments[$arg_position] eq "--token" ) or ( $arguments[$arg_position] eq "-t" ) ) {
			# Increment '$arg_position' by one
			$arg_position++;
			# Make sure '--token' value is not empty and matches RegEx for username
			if ( $arguments[$arg_position] !~ /^\s*\n*$/ ) {
				# If '--token' has already been set
				if ( $auth_params{'token'} ) {
					# Throw fatal error and exit
					die "FATAL ERROR: value for '--token' already set";
				}
				# Otherwise...
				else {
					# Add value for 'host' to 'auth_params' dict/hash
					$auth_params{'token'} = $arguments[$arg_position];
				} # End "If '--token' has already been set"
			} # End "Make sure '--token' value is not empty and matches RegEx for username"
		} # End "Game Auth Token"
		# New User
		elsif ( ( $arguments[$arg_position] eq "--new" ) or ( $arguments[$arg_position] eq "-n" ) ) {
			$auth_params{'new'} = "true";
		} # End "New User"
		# Handle all other (invalid) args
		else {
			# Throw non-fatal error
			print "NON-FATAL ERROR: ignoring unknown CLI argument --> '$arguments[$arg_position]'\n";
		} # End "Handle all other (invalid) args"
		# Increment '$arg_position' by one
		$arg_position++;
	} # End "Loop through all arguments"
	# Authenticate the user using specified params
	auth_user \%auth_params;
}

# # # # 
# name:		main
# purpose:	Entrypoint and main function for game client
# # # # 
sub main {
	arg_parse @_;
}

# Entrypoint
main @ARGV;
