#!/usr/bin/env perl

# Fail on error
use strict;
use warnings;

# The word with included letters in a known position
our @included_letters_position_known;

# The word with invalid letters in a known position 
our @invalid_letters_position_known;

# Other included letters with positions unknown
our @included_letters_position_unknown;

# List of all lower case chars
our @lower_case_chars = ('a'..'z');

# Array of excluded letters
our @excluded_letters;

# Array of generated words
our @possible_word_list;

# Array of filtered words
our @filtered_word_list;

# Path to a text file to use as a dictionary
our $path_to_dictionary;

# Print the help text
sub print_help {
	print "\n";
	print "NAME:\n";
	print "\n";
	print "\twordlecheat.pl\t\tPoorly written Perl script to generate possible\n";
	print "\t\t\t\tsolutions to a Wordle puzzle.\n";
	print "\n";
	print "AUTHORS:\n";
	print "\n";
	print "\tWritten by\t\tChris Blankenship <cblankenship\@pm.me>\n";
	print "\t\t\t\twww.chrisblankenship.lol\n";
	print "\n";
	print "NOTES:\n";
	print "\n";
	print "\tThis script is inefficient, clunky, and (as mentioned) poorly written. It\n";
	print "\tmostly exists as a personal learning exercise to become more comfortable\n";
	print "\twith writing Perl scripts. If you decide to use it (something I do not\n";
	print "\tsuggest), know that it will print out a lot of junk solutions. There are\n";
	print "\tsome options below to filter said output, but they're not super robust or\n";
	print "\tefficient. If you want better filtering try piping output to `grep -E`, or\n";
	print "\tjust, like, open a dictionary and do the puzzle like you're supposed to.\n";
	print "\n";
	print "OPTIONS:\n";
	print "\n";
	print "\t--word, -w\t\t'a.o.t'\n";
	print "\n";
	print "\t\tRequired. Only one instance allowed. As much of the word that\n";
	print "\t\tyou're trying to guess as you know, with unknown letters replaced\n";
	print "\t\t with the '.' char. Letters passed to this argument can also be passed\n";
	print "\t\tto the '--exclude' argument as well in order to keep them from being\n";
	print "\t\trepeated.\n";
	print "\n";
	print "\t--include, -i\t\t'bu'\n";
	print "\n";
	print "\t\tOptional. Only one instance allowed. Letters (up to 5) you know are\n";
	print "\t\tin the word you're trying to guess, but you don't know their exact\n";
	print "\t\tposition. Letters passed to '--include' cannot also be passed to '--exclude'\n";
	print "\t\tas well.\n";
	print "\n";
	print "\t--exclude, -e\t\t'hijklma'\n";
	print "\n";
	print "\t\tOptional. Only one instance allowed. Any letters (up to 21) you\n";
	print "\t\talready know aren't in the word you're trying to guess. Letters passed\n";
	print "\t\tto '--word' can be passed to '--exclude' here to prevent them from being\n";
	print "\t\trepeated. Letters passed to '--exclude' cannot also be passed to '--include'.\n";
	print "\n";
	print "\t--not, -n\t\t'b.u.a'\n";
	print "\n";
	print "\t\tOptional. Multiple instances allowed. Letters you know are not in a\n";
	print "\t\tspecific position, with unknown/known valid letters replaced with the\n";
	print "\t\t'.' char.\n";
	print "\n";
	print "\t--dictionary, -d\t'/path/to/list/of/words.txt'\n";
	print "\n";
	print "\t\tOptional. Only one instance allowed. Path to any (flat) text file with\n";
	print "\t\tone line per word. If such a file is passed to '--dictionary' only\n";
	print "\t\tgenerated words also present in this file will be printed. Any word\n";
	print "\t\tpresent in this file over 5 letters in length will be ignored.\n";
	print "\n";
	print "\t--help, -h\n";
	print "\n";
	print "\t\tPrints this help then exits. Will ignore any other arguments.\n";
	print "\n";
	print "EXAMPLES:\n";
	print "\n";
	print "\tperl wordlecheat.pl --word 'a.o.t'\n";
	print "\tperl wordlecheat.pl --word 'a.o.t' --include 'bu' --exclude 'hijklma' --not '.u...'\n";
	print "\tperl wordlecheat.pl --word '.....' --include 'abtou' --not '.au..' --not '..at.'\n";
	print "\tperl wordlecheat.pl --word 'a....' --exclude 'hijklma' --dictionary '/usr/share/dict/words'\n";
	print "\n";
	exit;
}

# Print final list of filtered words
sub print_words {
	# Access global array of filtered words
	our @filtered_word_list;
	# Loop over all possible (filtered) words
	foreach my $this_filtered_word ( @filtered_word_list ) {
		print "$this_filtered_word\n";
	}
}

# Filter generated words using included letters in unknown positions,
# invalid letters in a known position, and a dictionary file
sub filter_words {
	# Access global array of included letters with known positions
	our @included_letters_position_known;
	# Access global array of invalid letters in a known position 
	our @invalid_letters_position_known;
	# Access global array of other included letters with positions unknown
	our @included_letters_position_unknown;
	# Access global array of generated words
	our @possible_word_list;
	# Local array of words filtered by letters in invalid positions
	my @filtered_words_invalid_position;
	# Local array of words filtered by included letters in unknown positions
	my @filtered_words_included_unknown_position;
	# Local array of words filtered using a passed dictionary
	my @filtered_words_dictionary;
	# Access global array of filtered words
	our @filtered_word_list;
	# Access global var storing path of passed dictionary file
	our $path_to_dictionary;
	# Make sure all words fail to pass regex of word(s) passed to '--not', but first
	# check if anything was passed to '--not'
	if ( @invalid_letters_position_known ) {
		# Loop over all possible words
		foreach my $this_word_to_filter_by_invalid ( @possible_word_list ) {
			# DO NOT ADD flag
			my $do_not_add_invalid = 0;
			# Iterate through anything the invalid position array
			foreach my $this_invalid_position_word ( @invalid_letters_position_known ) {
				if ( $this_word_to_filter_by_invalid =~ /$this_invalid_position_word/ ) {
					$do_not_add_invalid = 1;
					last;
				}
			}
			# If DO NOT ADD flag has NOT been flipped for this word
			if ( $do_not_add_invalid == 0 ) {
				# Push it to the array of words filtered based on letters in invalid positions
				push( @filtered_words_invalid_position, $this_word_to_filter_by_invalid );
			}
		}
	}
	# If nothing was passed to '--not', just include everything in the list of words filtered
	# based on letters in invalid positions
	else {
		@filtered_words_invalid_position = @possible_word_list;
	}
	# If included chars were even passed
	if (@included_letters_position_unknown) {
		# Build hash of word counts
		my %included_letter_counts;
		# Loop through '@included_letters_position_unknown' to build '%included_letter_counts'
		foreach my $this_included_letter ( @included_letters_position_unknown ) {
			$included_letter_counts{$this_included_letter}++;
		}
		# Loop through '@included_letters_position_known' to increase count of letters that exist in both lists
		foreach my $this_included_letter_in_word ( @included_letters_position_known ) {
			if ( ( exists( $included_letter_counts{$this_included_letter_in_word} ) ) and  ( $this_included_letter_in_word ne "." ) ) {
				$included_letter_counts{$this_included_letter_in_word}++;
			}
		}
		# Loop over all possible words (filtered by previous check)
		foreach my $this_word_to_filter_by_included ( @filtered_words_invalid_position ) {
			# DO NOT ADD flag
			my $do_not_add_included = 0;
			# Loop over all included chars with unknown positions
			foreach my $this_included_letter ( @included_letters_position_unknown ) {
				# Construct regex to check word
				my $this_included_letter_count = $included_letter_counts{$this_included_letter};
				my $this_included_letter_regex = "$this_included_letter\{$this_included_letter_count,\}";
				# Temp value of word sorted alphabetically 
				my $temp_sorted_word = join( "", ( sort split( //, $this_word_to_filter_by_included ) ) );
				# Match regex against temp alphabetically sorted string
				if ( $temp_sorted_word !~ /$this_included_letter_regex/ ) {
					$do_not_add_included = 1;
					last;
				}
			}
			# If DO NOT ADD flag has NOT been flipped for this word
			if ( $do_not_add_included == 0 ) {
				# Push it to the (first) list of words filtered by regex
				push( @filtered_words_included_unknown_position, $this_word_to_filter_by_included );
			}
		}
	}
	# If nothing was passed to '--include', just include everything in the list of words filtered
	# based on included letters in unknown positions
	else {
		@filtered_words_included_unknown_position = @filtered_words_invalid_position;
	}
	# 
	# If a path to a dictionary was even passed
	if ( $path_to_dictionary ) {
		# Open passed file (which should exist)
		open ( my $dictionary_file_contents_raw, $path_to_dictionary );
		# Create array to store file contents
		my @dictionary_file_contents_list;
		# Loop through entire dictionary file
		while (my $this_dictionary_line = <$dictionary_file_contents_raw>) {
			# Sanitize var
			chomp $this_dictionary_line;
			# Only get words that are 5 chars in length
			if ( length($this_dictionary_line) == 6 ) {
				# Push (sanitized) line to array
				push @dictionary_file_contents_list, $this_dictionary_line;
			}
		}
		# Close dictionary file
		close $dictionary_file_contents_raw;
		# Loop over all possible words (filtered by previous check)
		foreach my $this_word_to_filter_by_dictionary ( @filtered_words_included_unknown_position ) {
			# If there's a match
			if (grep( /$this_word_to_filter_by_dictionary/, @dictionary_file_contents_list ) ) {
				# Push it to the proper array
				push @filtered_words_dictionary, $this_word_to_filter_by_dictionary;
			}
		}
	}
	# If nothing was passed to '--dictionary', just include everything in the list of words filtered
	# against a dictionary
	else { 
		@filtered_words_dictionary = @filtered_words_included_unknown_position;
	}
	# If that last list is not empty
	if ( @filtered_words_included_unknown_position  ) {
		# Make it equal to the final list of filtered words
		@filtered_word_list = @filtered_words_dictionary;
	}
	# But if it is, error out
	else {
		print "ERROR: list of filtered words empty...\n";
		exit;
	}
}

# Generate all permutations of the word using
# letters in known positions
sub generate_permutations {
	# Access global array of letters in known positions
	our @included_letters_position_known;
	# Access global array of other included letters with positions unknown
	our @included_letters_position_unknown;
	# Access global array of excluded letters
	our @excluded_letters;
	# Access global array of generated words
	our @possible_word_list;
	# Access list of all lower case chars
	our @lower_case_chars;
	# Array of possible solutions for letter 1
	my @letter_1_possible_solutions;
	# Letter 1
	if ( $included_letters_position_known[0] eq "." ) {
		# Loop over all lower case chars as replacements
		foreach my $letter_1_possible_char ( @lower_case_chars ) {
			# String w/ letter in question replaced
			my $this_letter_1_possible_solution = $letter_1_possible_char . $included_letters_position_known[1] . $included_letters_position_known[2] . $included_letters_position_known[3]. $included_letters_position_known[4];
			# Push to array of solutions
			push( @letter_1_possible_solutions, $this_letter_1_possible_solution);
		}
	}
	else { 
		# If letter in the position defined, push that one word to solutions array
		my $this_1_full_word = join( "", @included_letters_position_known );
		push( @letter_1_possible_solutions, $this_1_full_word );
	}
	# Array of possible solutions for letter 2
	my @letter_2_possible_solutions;
	# Letter 2
	if ( $included_letters_position_known[1] eq "." ) {
		# Loop over all solutions for previous letter
		foreach my $this_letter_1_possible_solution_string ( @letter_1_possible_solutions ) {
			my @this_letter_1_possible_solution_list = split( //, $this_letter_1_possible_solution_string );
			# Loop over all lower case chars as replacements
			foreach my $letter_2_possible_char ( @lower_case_chars ) {
				# String w/ letter in question replaced
				my $this_letter_2_possible_solution = $this_letter_1_possible_solution_list[0] . $letter_2_possible_char . $this_letter_1_possible_solution_list[2] . $this_letter_1_possible_solution_list[3] . $this_letter_1_possible_solution_list[4];
				# Push to array of solutions
				push( @letter_2_possible_solutions, $this_letter_2_possible_solution );
			}
		}
	}
	else { 
		# If letter in the position defined, move on
		@letter_2_possible_solutions = @letter_1_possible_solutions;
	}
	# Array of possible solutions for letter 3
	my @letter_3_possible_solutions;
	# Letter 3
	if ( $included_letters_position_known[2] eq "." ) {
		# Loop over all solutions for previous letter
		foreach my $this_letter_2_possible_solution_string ( @letter_2_possible_solutions ) {
			my @this_letter_2_possible_solution_list = split( //, $this_letter_2_possible_solution_string );
			# Loop over all lower case chars as replacements
			foreach my $letter_3_possible_char ( @lower_case_chars ) {
				# String w/ letter in question replaced
				my $this_letter_3_possible_solution = $this_letter_2_possible_solution_list[0] . $this_letter_2_possible_solution_list[1] . $letter_3_possible_char . $this_letter_2_possible_solution_list[3] . $this_letter_2_possible_solution_list[4];
				# Push to array of solutions
				push( @letter_3_possible_solutions, $this_letter_3_possible_solution );
			}
		}
	}
	else { 
		# If letter in the position defined, move on
		@letter_3_possible_solutions = @letter_2_possible_solutions;
	}
	# Array of possible solutions for letter 4
	my @letter_4_possible_solutions;
	# Letter 4
	if ( $included_letters_position_known[3] eq "." ) {
		# Loop over all solutions for previous letter
		foreach my $this_letter_3_possible_solution_string ( @letter_3_possible_solutions ) {
			my @this_letter_3_possible_solution_list = split( //, $this_letter_3_possible_solution_string );
			# Loop over all lower case chars as replacements
			foreach my $letter_4_possible_char ( @lower_case_chars ) {
				# String w/ letter in question replaced
				my $this_letter_4_possible_solution = $this_letter_3_possible_solution_list[0] . $this_letter_3_possible_solution_list[1] . $this_letter_3_possible_solution_list[2] . $letter_4_possible_char . $this_letter_3_possible_solution_list[4];
				# Push to array of solutions
				push( @letter_4_possible_solutions, $this_letter_4_possible_solution );
			}
		}
	}
	else { 
		# If letter in the position defined, move on
		@letter_4_possible_solutions = @letter_3_possible_solutions;
	}
	# Array of possible solutions for letter 5
	my @letter_5_possible_solutions;
	# Letter 5
	if ( $included_letters_position_known[4] eq "." ) {
		# Loop over all solutions for previous letter
		foreach my $this_letter_4_possible_solution_string ( @letter_4_possible_solutions ) {
			my @this_letter_4_possible_solution_list = split( //, $this_letter_4_possible_solution_string );
			# Loop over all lower case chars as replacements
			foreach my $letter_5_possible_char ( @lower_case_chars ) {
				# String w/ letter in question replaced
				my $this_letter_5_possible_solution = $this_letter_4_possible_solution_list[0] . $this_letter_4_possible_solution_list[1] . $this_letter_4_possible_solution_list[2] . $this_letter_4_possible_solution_list[3] . $letter_5_possible_char;
				# Push to array of solutions
				push( @letter_5_possible_solutions, $this_letter_5_possible_solution );
			}
		}
	}
	else { 
		# If letter in the position defined, move on
		@letter_5_possible_solutions = @letter_4_possible_solutions;
	}
	# if array is not empty, pass the final list of possible solutions to the global list
	if ( @letter_5_possible_solutions ){
		@possible_word_list = @letter_5_possible_solutions;
	}
	else {
		print "ERROR: no permutations generated...\n";
		exit;
	}
}

# Remove excluded chars from lower case word list
sub remove_excluded {
	# Access global array of other included letters with positions unknown
	our @included_letters_position_unknown;
	# Access global array of excluded letters
	our @excluded_letters;
	# Access list of all lower case chars
	our @lower_case_chars;
	# If excluded chars were even passed
	if ( @excluded_letters ) {
		# Iterate through list of excluded chars
		foreach my $this_excluded_letter ( @excluded_letters ) {
			# Remove excluded chars from list of all lower case chars
			@lower_case_chars = grep( !/$this_excluded_letter/, @lower_case_chars );
		}
	}
	# If number of included letters is 5 (which is the max length)
	if ( @included_letters_position_unknown == 5 ) {
		# There's no point in having any other chars in the list of lower case chars
		@lower_case_chars = sort @included_letters_position_unknown;
	}
}

# Parse arguments
sub arg_parse { 
	# Flag to exit, '0' is do not exit and '1' is do exit
	my $to_exit = 0;
	# Flag if there was a failure setting the required argument ('--word')
	my $required_failed = 0;
	# Current position in arguments list
	my $arg_position = 0;
	# List of params (should be ARGV)
	my @arguments = @_;
	# Access global array of letters with known positions
	our @included_letters_position_known;
	# Access global array of invalid letters in a known position 
	our @invalid_letters_position_known;
	# Access global array of other included letters with positions unknown
	our @included_letters_position_unknown;
	# Access global array of excluded letters
	our @excluded_letters;
	# Check if no arguments were passed
	if ( !@arguments ) {
		print "ERROR: no arguments passed...\n";
		$to_exit = 1;
	}
	else {
		# Loop through arguments
		while ( @arguments > $arg_position ) { 
			# The word with letters in a known position
			if ( ( $arguments[$arg_position] eq "--word" ) or ( $arguments[$arg_position] eq "-w" ) ){
				# If '@included_letters_position_known' is still empty
				if ( !@included_letters_position_known ) {
					# Increment '$arg_position' by one
					$arg_position++;
					# Check if it adheres to the expected RegEx (any 5 lower case alphabetical or '.' chars)
					if ( lc( $arguments[$arg_position] ) =~ /^[a-z\.]{5}$/ ) {
						# Place the value in the appropriate array
						@included_letters_position_known = split( //, lc( $arguments[$arg_position] ) );
					}
					else {
						print "ERROR: value for argument '--word' is not valid...\n";
						$to_exit = 1;
						$required_failed = 1;
					}
				}
				else {
					print "ERROR: value for argument '--word' already passed...\n";
					$to_exit = 1;
				}
			}
			# The word with invalid letters in a known position
			elsif ( ( $arguments[$arg_position] eq "--not" ) or ( $arguments[$arg_position] eq "-n" ) ){
				# Increment '$arg_position' by one
				$arg_position++;
				# Check if it adheres to the expected RegEx (any 5 lower case alphabetical or '.' chars)
				if ( lc( $arguments[$arg_position] ) =~ /^[a-z\.]{5}$/ ) {
					# Place the value in the appropriate array
					push( @invalid_letters_position_known, $arguments[$arg_position] );
				}
				else {
					print "ERROR: value for argument '--not' is not valid...\n";
					$to_exit = 1;
					$required_failed = 1;
				}
			}
			# Included letters in unknown positions
			elsif ( ( $arguments[$arg_position] eq "--include" ) or ( $arguments[$arg_position] eq "-i" ) ) { 
				# If '@included_letters_position_unknown' is still empty
				if ( !@included_letters_position_unknown ) {
					# Increment '$arg_position' by one
					$arg_position++;
					# Check if it adheres to the expected RegEx (any lower case alphabetical char)
					if ( lc( $arguments[$arg_position] ) =~ /^[a-z]{1,5}$/ ) {
						# Place the value in the appropriate array
						@included_letters_position_unknown = split( //, lc( $arguments[$arg_position] ) );
					}
					else {
						print "ERROR: value for argument '--include' is not valid...\n";
						$to_exit = 1;
					}
				}
				else {
					print "ERROR: value for argument '--include' already passed...\n";
					$to_exit = 1;
				}
			}
			# Excluded letters
			elsif ( ( $arguments[$arg_position] eq "--exclude" ) or ( $arguments[$arg_position] eq "-e" ) ) {
				# If '@excluded_letters' is still empty
				if ( !@excluded_letters ) {
					# Increment '$arg_position' by one
					$arg_position++;
					# Check if it adheres to the expected RegEx (any lower case alphabetical char)
					if ( lc( $arguments[$arg_position] ) =~ /^[a-z]{1,21}$/ ) {
						# Place the value in the appropriate array
						@excluded_letters = split( //, lc( $arguments[$arg_position] ) );
					}
					else {
						print "ERROR: value for argument '--exclude' is not valid...\n";
						$to_exit = 1;
					}
				}
				else {
					print "ERROR: value for argument '--exclude' already passed...\n";
					$to_exit = 1;
				}
			}
			# Dictionary file
			elsif ( ( $arguments[$arg_position] eq "--dictionary" ) or ( $arguments[$arg_position] eq "-d" ) ) {
				# If '$path_to_dictionary' is still empty
				if ( !$path_to_dictionary ) {
					# Increment '$arg_position' by one
					$arg_position++;
					# Check if passed file actually exists
					if ( -f $arguments[$arg_position] ) {
						# Place the value in the appropriate array
						$path_to_dictionary = $arguments[$arg_position];
					}
					else {
						print "ERROR: file at path passed for '--dictionary' does not exist...\n";
						$to_exit = 1;
					}
				}
				else {
					print "ERROR: value for argument '--dictionary' already passed...\n";
					$to_exit = 1;
				}
			}
			# Print help
			elsif ( ( $arguments[$arg_position] eq "--help" ) or ( $arguments[$arg_position] eq "-h" ) ) {
				print_help;
			}
			# Handle invalid arguments
			else {
				print "ERROR: '$arguments[$arg_position]' is not a valid argument...\n";
				$to_exit = 1;
			}
			# Increment '$arg_position' by one
			$arg_position++;
		}
		# Check if required arg ('--word') exists, and if it only doesn't because
		# the input was invalid
		if ( ( !@included_letters_position_known ) and ( $required_failed == 0 ) ) {
			print "ERROR: no argument '--word' passed...\n";
			$to_exit = 1;
		}
		# Check if included letter (unknown position) also exists in excluded list
		foreach my $test_included_unknown_char ( @included_letters_position_unknown ) {
			if ( ( grep( /$test_included_unknown_char/, @excluded_letters ) ) ) {
				print "ERROR: included letter also in excluded list...\n";
				$to_exit = 1;
			}
		}
		# Check if excluded letter also exists in a included list (unknown position)
		foreach my $test_excluded_char ( @excluded_letters ) {
			if ( grep( /$test_excluded_char/, @included_letters_position_unknown ) ){
				print "ERROR: excluded letter also in included list...\n";
				$to_exit = 1;
			}
		}
		# Check if any letter passed to '--word' was also passed to an instance of '--not'
		# by first checking if anything was passed to '--not'
		if ( @invalid_letters_position_known ) {
			# Iterate through anything in that array
			foreach my $this_invalid_position_word ( @invalid_letters_position_known ) {
				# Split each word in that array into its own array
				my @test_invalid_position_char_list = split( //, $this_invalid_position_word );
				# Compare chars between '--not' and '--word'
				if ( ( $test_invalid_position_char_list[0] eq $included_letters_position_known[0]) and ( $test_invalid_position_char_list[0] ne "." ) ) {
					print "ERROR: valid and invalid letter in same position (1st letter)...\n";
					$to_exit = 1;
				}
				if ( ( $test_invalid_position_char_list[1] eq $included_letters_position_known[1]) and ( $test_invalid_position_char_list[1] ne "." ) ) {
					print "ERROR: valid and invalid letter in same position (2nd letter)...\n";
					$to_exit = 1;
				}
				if ( ( $test_invalid_position_char_list[2] eq $included_letters_position_known[2]) and ( $test_invalid_position_char_list[2] ne "." ) ) {
					print "ERROR: valid and invalid letter in same position (3rd letter)...\n";
					$to_exit = 1;
				}
				if ( ( $test_invalid_position_char_list[3] eq $included_letters_position_known[3]) and ( $test_invalid_position_char_list[3] ne "." ) ) {
					print "ERROR: valid and invalid letter in same position (4th letter)...\n";
					$to_exit = 1;
				}
				if ( ( $test_invalid_position_char_list[4] eq $included_letters_position_known[4]) and ( $test_invalid_position_char_list[4] ne "." ) ) {
					print "ERROR: valid and invalid letter in same position (5th letter)...\n";
					$to_exit = 1;
				}
			}
		}
	}
	# Check if fail flag was switched
	if ( $to_exit == 1 ){
		print_help;
	}
}

# Entrypoint
sub main {
	arg_parse @_;
	remove_excluded;
	generate_permutations;
	filter_words;
	print_words;
}

main @ARGV;
